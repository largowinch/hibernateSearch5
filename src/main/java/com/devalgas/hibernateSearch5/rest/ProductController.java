package com.devalgas.hibernateSearch5.rest;

import com.devalgas.hibernateSearch5.domain.Product;
import com.devalgas.hibernateSearch5.repository.ProductSearchRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author devalgas kamga on 19/07/2021. 11:29
 */
@RestController
@RequestMapping("/api")
public class ProductController {

    private final ProductSearchRepository productSearchRepository;

    public ProductController(ProductSearchRepository productSearchRepository) {
        this.productSearchRepository = productSearchRepository;
    }

    @GetMapping("/product/keywordQuery/{text}")
    public List<Product> searchProductNameByKeywordQuery(@PathVariable("text") String text) {
        return productSearchRepository.searchProductNameByKeywordQuery(text);
    }

    @GetMapping("/product/fuzzyQuery/{text}")
    public List<Product> searchProductNameByFuzzyQuery(@PathVariable("text") String text) {
        return productSearchRepository.searchProductNameByFuzzyQuery(text);
    }

    @GetMapping("/product/wildcardQuery/{text}")
    public List<Product> searchProductNameByWildcardQuery(@PathVariable("text") String text) {
        return productSearchRepository.searchProductNameByWildcardQuery(text);
    }

    @GetMapping("/product/phraseQuery/{text}")
    public List<Product> searchProductDescriptionByPhraseQuery(@PathVariable("text") String text) {
        return productSearchRepository.searchProductDescriptionByPhraseQuery(text);
    }

    @GetMapping("/product/queryStringQuery/{text}")
    public List<Product> searchProductNameAndDescriptionBySimpleQueryStringQuery(@PathVariable("text") String text) {
        return productSearchRepository.searchProductNameAndDescriptionBySimpleQueryStringQuery(text);
    }

    @GetMapping("/product/rangeQuery/{low}/{high}")
    public List<Product> searchProductNameByRangeQuery(@PathVariable("low") int low, @PathVariable("high") int high) {
        return productSearchRepository.searchProductNameByRangeQuery(low, high);
    }

    @GetMapping("/product/moreLikeThisQuery")
    public List<Object[]> searchProductNameByMoreLikeThisQuery(@RequestBody Product Product) {
        return productSearchRepository.searchProductNameByMoreLikeThisQuery(Product);
    }

    @GetMapping("/product/keywordQuery2/{text}")
    public List<Product> searchProductNameAndDescriptionByKeywordQuery(@PathVariable("text") String text) {
        return productSearchRepository.searchProductNameAndDescriptionByKeywordQuery(text);
    }

    @GetMapping("/product/moreLikeThisQuery2")
    public List<Object[]> searchProductNameAndDescriptionByMoreLikeThisQuery(@RequestBody Product Product) {
        return productSearchRepository.searchProductNameAndDescriptionByMoreLikeThisQuery(Product);
    }

    @GetMapping("/product/combinedQuery/{manufactorer}/{memoryLow}/{memoryTop}/{extraFeature}/{exclude}")
    public List<Product> searchProductNameAndDescriptionByCombinedQuery(@PathVariable("manufactorer") String manufactorer,@PathVariable("memoryLow") int memoryLow,@PathVariable("memoryTop") int memoryTop,@PathVariable("extraFeature") String extraFeature,@PathVariable("exclude") String exclude) {

        return productSearchRepository.searchProductNameAndDescriptionByCombinedQuery(manufactorer, memoryLow, memoryTop, extraFeature,exclude);
    }

    }
