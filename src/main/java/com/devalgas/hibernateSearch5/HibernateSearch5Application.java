package com.devalgas.hibernateSearch5;

import com.devalgas.hibernateSearch5.domain.Product;
import com.devalgas.hibernateSearch5.repository.ProductRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HibernateSearch5Application implements CommandLineRunner {

	private final ProductRepository productRepository;

	public HibernateSearch5Application(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}


	public static void main(String[] args) {
		SpringApplication.run(HibernateSearch5Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		productRepository.deleteAll();

		Product product1 = new Product();
		product1.setProductName("toyota aa11");
		product1.setDescription("voiture toyota aa11 rouge 1923");
		product1.setMemory(1923);

		Product product2 = new Product();
		product2.setProductName("land rover aa12");
		product2.setDescription("voiture land rover aa12 rouge 1924");
		product2.setMemory(1924);

		Product product3 = new Product();
		product3.setProductName("nissan aa13");
		product3.setDescription("voiture nissan aa13 vert 1925");
		product3.setMemory(1925);

		Product product4 = new Product();
		product4.setProductName("open rouge");
		product4.setDescription("voiture open aa14 rouge 1926");
		product4.setMemory(1926);
		productRepository.save(product1);
		productRepository.save(product2);
		productRepository.save(product3);
		productRepository.save(product4);


	}
}
