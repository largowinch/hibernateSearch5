package com.devalgas.hibernateSearch5.repository;

import com.devalgas.hibernateSearch5.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author devalgas kamga on 19/07/2021. 11:27
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, String> {
}
